#ifndef NGL_H
#define NGL_H

#include "ShaderProgram.h"

#include <iostream>
#include <string>

#include <GL/glew.h>
#include <GL/glfw3.h>

class NGL{
public:
	// Constructors
	NGL();
	NGL(int width, int height);
	NGL(int width, int height, int maxFps);
	NGL(int width, int height, int maxFps, bool fullscreen);
	NGL(int width, int height, int maxFps, int antialiasSamples);
	NGL(int width, int height, int maxFps, int antialiasSamples, bool fullscreen);

	// Destructor
	~NGL();

	// Publics methods
	void render();
	void loop(void (*callback)(double, double));

	// Setters
	void setMaxFPS(int maxFPS);

	// Getters
	double getDelta() const;
	double getFPS() const;
	
private:
	// Private methods
	void init();
	void initGL();
	
	static void errorCallback(int error, const char* desc);
	static void closeCallback(GLFWwindow* window);
	static void keyCallback(GLFWwindow* window, int key, int scancode, int action, int mods);

	// Shader programs
	ShaderProgram m_materialProgram;

	// OpenGL buffers
	GLuint m_vertexBuffer;
	GLuint m_vertexArray;

	// Members
	GLFWwindow* m_window;

	bool m_fullscreen;

	double m_delta;
	double m_currentFPS;

	int m_maxFPS;
	int m_ANTIALIAS_SAMPLES;
	int m_width;
	int m_height;
};

#endif // NGL_H
