#include <iostream>

#include <GL/glew.h>
#include <GL/glfw3.h>

using namespace std;

// VARIABlES
extern bool FULLSCREEN = false;
extern int ANTIALIAS_SAMPLES = 4;

extern GLFWwindow* window;
extern int width, height;

// CALLBACKS
// Error callback
void errorCallback(int error, const char* desc){
	fputs(desc, stderr);
}

// Close callback
void closeCallback(GLFWwindow* window){
	// on close
}

// Key callback
void keyCallback(GLFWwindow* window, int key, int scancode, int action, int mods){
	if(key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
		glfwSetWindowShouldClose(window, GL_TRUE);
}

// Resize callback
void resizeCallback(GLFWwindow* window, int w, int h){
	width = w;
	height = h;
}

// FUNCTIONS
void NGL_setCallbacks(){
	glfwSetErrorCallback(errorCallback);

	glfwSetWindowCloseCallback(window, closeCallback);
	glfwSetKeyCallback(window, keyCallback);
	glfwSetFramebufferSizeCallback(window, resizeCallback);
}

void NGL_loop(){
	while(!glfwWindowShouldClose(window)){
		glfwSwapBuffers(window);
		glfwPollEvents();
	}
}

void NGL_initWindow(bool fullscreen){
	glfwWindowHint(GLFW_SAMPLES, ANTIALIAS_SAMPLES);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);

	if(fullscreen){
		GLFWmonitor* monitor = glfwGetPrimaryMonitor();
		const GLFWvidmode* mode = glfwGetVideoMode(monitor);

		window = glfwCreateWindow(mode->width, mode->height, "Tests", monitor, NULL);
	}else{
		window = glfwCreateWindow(1280, 720, "Tests", NULL, NULL);
	}

	glfwSetInputMode(window, GLFW_STICKY_KEYS, GL_TRUE);
	NGL_setCallbacks();

	if(!window){
		glfwTerminate();
		exit(EXIT_FAILURE);
	}
}

void NGL_initGLEW(){
	glfwMakeContextCurrent(window);
	glewExperimental = true;

	if(glewInit() != GLEW_OK){
		cout << "Failed to initialize GLEW: " << stderr << endl;
		exit(EXIT_FAILURE);
	}
}

void NGL_start(){
	NGL_initWindow(FULLSCREEN);
	NGL_initGLEW();

	NGL_loop();

	glfwDestroyWindow(window);
}

int NGL_init(){
	if(!glfwInit()){
		cout << "Failed to init glfw!" << endl;
		return -1;
	}

	NGL_start();

	glfwTerminate();

	return 0;
}
