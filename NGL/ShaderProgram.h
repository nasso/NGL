#ifndef SHADER_PROGRAM_H
#define SHADER_PROGRAM_H

#include <GL/glew.h>
#include <string>

class ShaderProgram{
public:
	// Constructor/Destructor
	ShaderProgram(std::string const& vertexFile, std::string const& fragmentFile);
	ShaderProgram();
	~ShaderProgram();

	// Public methods
	GLuint progID() const;

	// Static methods
	static GLuint loadShader(std::string const& sourceFile, GLuint const& type);
	static GLuint createProgram(GLuint const& vertexShader, GLuint const& fragmentShader);
	static void disposeProgram(GLuint const& program, GLuint const& vertexShader, GLuint const& fragmentShader);
private:
	GLuint m_vertexShader;
	GLuint m_fragmentShader;

	GLuint m_program;
};

#endif // SHADER_PROGRAM_H
