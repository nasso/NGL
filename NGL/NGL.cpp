#include "NGL.h"

using namespace std;

// Constructors
NGL::NGL() : m_width(800), m_height(600), m_ANTIALIAS_SAMPLES(4), m_maxFPS(0), m_fullscreen(false) {
	this->init();
}

NGL::NGL(int width, int height) : m_width(width), m_height(height), m_ANTIALIAS_SAMPLES(4), m_maxFPS(0), m_fullscreen(false) {
	this->init();
}

NGL::NGL(int width, int height, int maxFps) : m_width(width), m_height(height), m_ANTIALIAS_SAMPLES(4), m_maxFPS(maxFps), m_fullscreen(false) {
	this->init();
}

NGL::NGL(int width, int height, int maxFps, bool fullscreen) : m_width(width), m_height(height), m_ANTIALIAS_SAMPLES(4), m_maxFPS(maxFps), m_fullscreen(fullscreen) {
	this->init();
}

NGL::NGL(int width, int height, int maxFps, int antialiasSamples) : m_width(width), m_height(height), m_ANTIALIAS_SAMPLES(antialiasSamples), m_maxFPS(maxFps), m_fullscreen(false) {
	this->init();
}

NGL::NGL(int width, int height, int maxFps, int antialiasSamples, bool fullscreen) : m_width(width), m_height(height), m_ANTIALIAS_SAMPLES(antialiasSamples), m_maxFPS(maxFps), m_fullscreen(fullscreen) {
	this->init();
}

// Destructor
NGL::~NGL(){
	glfwDestroyWindow(m_window);

	glfwTerminate();
}

// Setters
void NGL::setMaxFPS(int maxFPS){
	if(maxFPS < 0){
		maxFPS = m_maxFPS;
	}

	m_maxFPS = maxFPS;
}

// Getters
double NGL::getDelta() const{
	return m_delta;
}

// Methods
void NGL::initGL(){
	/*
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);

	glEnable(GL_CULL_FACE);
	*/

	glClearColor(0.0, 0.0, 1.0, 1.0);

	// INIT PROGRAMS
	m_materialProgram = ShaderProgram(
		"shaders/material.vs",
		"shaders/material.fs");

	// INIT BUFFERS
	static const GLfloat vertices[] = {
		-1.0f, -1.0f, 0.0f,
		1.0f, -1.0f, 0.0f,
		0.0f,  1.0f, 0.0f
	};

	glGenVertexArrays(1, &m_vertexArray);
	glBindVertexArray(m_vertexArray);

	glGenBuffers(1, &m_vertexBuffer);
	glBindBuffer(GL_ARRAY_BUFFER, m_vertexBuffer);
	glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);
}

void NGL::render(){
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glUseProgram(m_materialProgram.progID());

	glEnableVertexAttribArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, m_vertexBuffer);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, (void*)0);
	glDrawArrays(GL_TRIANGLES, 0, 3);
	glDisableVertexAttribArray(0);

	glUseProgram(0);
}

void NGL::loop(void (*callback)(double, double)){
	double lastUpdateTime = glfwGetTime()/1000;
	double currentTime = lastUpdateTime;

	double lastFPSUpdateTime = currentTime;
	int nbFrame = 0;

	while(!glfwWindowShouldClose(m_window)){
		currentTime = glfwGetTime()/1000;
		m_delta = lastUpdateTime - currentTime;

		callback(currentTime, m_delta);
		render();

		glfwSwapBuffers(m_window);
		glfwPollEvents();

		nbFrame++;
		if(currentTime-lastFPSUpdateTime >= 1){
			m_currentFPS = nbFrame;
			nbFrame = 0;
			lastFPSUpdateTime = currentTime;
		}

		/*
		if(m_maxFPS != 0){
			double targetTimeBetweenFrames = 1000/m_maxFPS;
			double currentFrameTime = (glfwGetTime()/1000) - currentTime;

			if(currentFrameTime < targetTimeBetweenFrames){
				double timeThatWeShouldWait = targetTimeBetweenFrames-currentFrameTime;

				// HERE WE SHOULD SLEEP FOR timeThatWeShouldWait MILLISECONDS
			}
		}
		*/
	}
}

// Callbacks
void NGL::errorCallback(int error, const char* desc){
	fputs(desc, stderr);
}
void NGL::closeCallback(GLFWwindow* window){

}
void NGL::keyCallback(GLFWwindow* window, int key, int scancode, int action, int mods){
	if(key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
		glfwSetWindowShouldClose(window, GL_TRUE);
}

void NGL::init(){
	if(!glfwInit()){
		throw new exception("Failed to init glfw!");
	}

	// INIT WINDOW
	glfwWindowHint(GLFW_SAMPLES, m_ANTIALIAS_SAMPLES);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);

	if(m_fullscreen){
		GLFWmonitor* monitor = glfwGetPrimaryMonitor();
		const GLFWvidmode* mode = glfwGetVideoMode(monitor);

		m_window = glfwCreateWindow(mode->width, mode->height, "Tests", monitor, NULL);
	}else{
		m_window = glfwCreateWindow(1280, 720, "Tests", NULL, NULL);
	}

	glfwSetInputMode(m_window, GLFW_STICKY_KEYS, GL_TRUE);

	if(!m_window){
		glfwTerminate();
		exit(EXIT_FAILURE);
	}

	// SET CALLBACKS
	glfwSetErrorCallback(errorCallback);

	glfwSetWindowCloseCallback(m_window, closeCallback);
	glfwSetKeyCallback(m_window, keyCallback);

	// INIT GLEW
	glfwMakeContextCurrent(m_window);
	glewExperimental = true;

	if(glewInit() != GLEW_OK){
		throw new exception("Failed to init GLEW");
	}

	// INIT GL
	initGL();
}
