#include "ShaderProgram.h"

#include <GL/glew.h>
#include <fstream>
#include <iostream>
#include <vector>

using namespace std;

// Static
GLuint ShaderProgram::loadShader(string const& sourceFile, GLuint const& type){
	if(type != GL_VERTEX_SHADER && type != GL_FRAGMENT_SHADER){
		return 0;
	}
	
	GLuint shader = glCreateShader(type);
	
	string source = "";

	ifstream fileStream(sourceFile);
	if(fileStream.is_open()){
		string line;
		cout << "Reading " << sourceFile << "..." << endl;
		while(getline(fileStream, line)){
			source += line + "\n";
		}
		fileStream.close();
	}else{
		cout << "Couldn't open " << sourceFile << "." << endl;
		return 0;
	}
	
	GLint result = GL_FALSE;
	GLint infoLogLength;

	char const *sourcePointer = source.c_str();
	glShaderSource(shader, 1, &sourcePointer, NULL);
	glCompileShader(shader);

	glGetShaderiv(shader, GL_COMPILE_STATUS, &result);
	glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &infoLogLength);
	if(infoLogLength > 0){
		vector<char> errorMessage(infoLogLength+1);

		glGetShaderInfoLog(shader, infoLogLength, NULL, &errorMessage[0]);
		cout << &errorMessage[0] << endl;
	}
}

GLuint ShaderProgram::createProgram(GLuint const& vertexShader, GLuint const& fragmentShader){
	GLuint program = glCreateProgram();
	glAttachShader(program, vertexShader);
	glAttachShader(program, fragmentShader);
	glLinkProgram(program);

	GLint result;
	GLint infoLogLength;

	glGetProgramiv(program, GL_LINK_STATUS, &result);
	glGetProgramiv(program, GL_INFO_LOG_LENGTH, &infoLogLength);
	if(infoLogLength > 0){
		vector<char> errorMessage(infoLogLength+1);
		glGetProgramInfoLog(program, infoLogLength, NULL, &errorMessage[0]);
		cout << &errorMessage[0] << endl;
	}

	return program;
}

void ShaderProgram::disposeProgram(GLuint const& program, GLuint const& vertexShader, GLuint const& fragmentShader){
	glDetachShader(program, vertexShader);
	glDetachShader(program, fragmentShader);

	glDeleteShader(vertexShader);
	glDeleteShader(fragmentShader);

	glDeleteProgram(program);
}

// Default constructor
ShaderProgram::ShaderProgram() : m_program(0), m_vertexShader(0), m_fragmentShader(0) {
	// Do nothing
}

// Constructor
ShaderProgram::ShaderProgram(string const& vertexFile, string const& fragmentFile){
	m_vertexShader = ShaderProgram::loadShader(vertexFile, GL_VERTEX_SHADER);
	m_fragmentShader = ShaderProgram::loadShader(fragmentFile, GL_FRAGMENT_SHADER);

	m_program = ShaderProgram::createProgram(m_vertexShader, m_fragmentShader);
}

// Destructor
ShaderProgram::~ShaderProgram(){
	ShaderProgram::disposeProgram(m_program, m_vertexShader, m_fragmentShader);
}

// Methods
GLuint ShaderProgram::progID() const{
	return m_program;
}
