#version 330 core

in vec3 vertexPosition_modelspace;

void main(){
	vec3 position_screenspace = vec3(
		vertexPosition_modelspace.x+0.1,
		vertexPosition_modelspace.y+0.1,
		vertexPosition_modelspace.z+0.1
	);
	
	gl_Position = vec4(position_screenspace, 1.0);
}
